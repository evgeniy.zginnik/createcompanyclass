
function newCompany() {
    window.company = new Company('Turboatom', 6);

    CreateTableCompany.createTable(company);
    window.company.registerUserCreateCallback((name) => CreateTableCompany.createUserHandler(name));
    window.company.registerNoSpaceNotifyer(user => CreateTableCompany.noSpaceHandler(user));
    window.company.registerUserUpdateCallback(user => {
        CreateTableCompany.changeUserHandler(user);
        CreateTableCompany.deleteUserHandler(user);
    });

    window.admin = Company.createSuperAdmin(company);
    window.first = admin.createUser('John', 'Hardy');
    window.admin.createUser('John', 'Hardy');
    window.third = admin.createUser('John', 'Hardy');
    window.admin.createUser('John', 'Hardy');

    CreateTableCompany.createUser

}
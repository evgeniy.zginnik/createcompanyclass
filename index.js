let Company = (function () {
    const adminPassword = Symbol('AdminIsReal!');
    const token = Symbol('tokenAdmin');
    const role = Symbol('role');
    const checkUser = Symbol('checkUser');
    const userCreateCallback = Symbol('registerUserCreateCallback')
    const userUpdateCallback = Symbol('registerUserCreateCallback')
    const noSpaceNotifyer = Symbol('registerUserCreateCallback')
    const id = Symbol('idForUser')

    const firstName = Symbol('name');
    const lastName = Symbol('lastName');
    const listUsers = Symbol('listUser')
    let usersList = [];

    const superToken = 'super__token';
    const adminToken = 'admin__token';

    class User {
        constructor(name, lastname, isSuper = false) {
            this[firstName] = name;
            this[lastName] = lastname;
            this[id] = Math.floor(Math.random() * 10000);
            this[role] = 'User';
            this[listUsers] = usersList;
            if (isSuper === true) {
              this[role] = 'SuperUser';
              this[token] = superToken;

              this.createUser = (name, lastnameUser) => {
                this[checkUser]();
                if(usersList.length === this.company.maxUser - 1) {
                  this.company[noSpaceNotifyer].forEach(i => i(`You created last user!`))
                }
                if (usersList.length === this.company.maxUser) { 
                  throw Error("User list is full!");
                };

                const newUser = new User(name, lastnameUser, false);

                this[userUpdateCallback] = this.company[userUpdateCallback];
                newUser[userUpdateCallback] = this[userUpdateCallback];
                this[userCreateCallback] = this.company[userCreateCallback];
                newUser[userCreateCallback] = this[userCreateCallback];

                usersList.push(newUser);

                this.company[noSpaceNotifyer].forEach(i => i(`Welcome new user - ${newUser.getName}!`))
                newUser[userCreateCallback].forEach(i => i(newUser))
                return newUser;
              }

              this.deleteUser = (id) => {
                this[checkUser]();
                const findUser = usersList.find(i => i.getUserId === id);
                if(!findUser) {
                    throw new Error('User is undefined!');
                };
                if(findUser[token]) {
                    throw new Error("Can't delete admin");
                };

                usersList = usersList.filter(i => i.getUserId !== id);
                
                this.company[noSpaceNotifyer].forEach(i => i(`User - ${findUser.getName} left the company!`))
                this[userUpdateCallback].forEach((i) => i(findUser.getUserId, this));
              }

          }
        }

        get getRole() {
          return this[role];
        }

        get getName() {
          return this[firstName];
        }

        get getLastName() {
          return this[lastName];
        }

        get getUserId() {
          return this[id];
        }

        set changeRole(newRole) {
          this[role] = newRole;
          this[userUpdateCallback].forEach(i => i(this));
        }

        set changeName(newName) {
          this[firstName] = newName;
          this[userUpdateCallback].forEach(i => i(this));
        }

        set changeLastName(newLastName) {
          this[lastName] = newLastName;
          this[userUpdateCallback].forEach(i => i(this));
        }

        [checkUser] = () => {
            if (this[token] !== adminToken) { 
                throw new Error('You dont have access for this!');
            };
            if (prompt('Enter password', '1111') !== this[adminPassword]) { 
                throw new Error('Wrong password!');
            };
        }

    }


    class Company {

        constructor(name, maxUser) {
          this.name = name;
          this.maxUser = maxUser;

          this[userCreateCallback] = [];
          this[userUpdateCallback] = [];
          this[noSpaceNotifyer] = [];
        };

        static createSuperAdmin(company) {
          if (usersList[0]) { throw Error("We can't have more than 1 Admin") };
          const adminAcc = new User('Admin', 'ADMIN', true);

          adminAcc[adminPassword] = prompt(`Create password for ${company.name} - admin`, "1111");
          if(adminAcc[adminPassword].length < 4) { throw Error('Password is too short') };
          adminAcc.company = company;
          usersList.unshift(adminAcc);
          adminAcc[token] = adminToken;

          return adminAcc;
        };


        registerUserCreateCallback(cb) {
          this[userCreateCallback].push(cb);
        }

        registerUserUpdateCallback(cb) {
          this[userUpdateCallback].push(cb);
        }

        registerNoSpaceNotifyer(cb) {
          this[noSpaceNotifyer].push(cb);
        }

        getUser(id) {
          return usersList.find(i => i.id === id)
        }
        get getListUsers() {
          return usersList
        }
        get getCurrentUsers() {
          return usersList.length
        }
    }

    return Company
}());
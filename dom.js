class CreateTableCompany {
    static userList = []
    
    static createTable(company) {
        document.body.insertAdjacentHTML('afterbegin', `
            <div class="container" style="border: 1px solid grey; padding: 0;">
                <nav class="navbar navbar-light bg-light">
                    <a class="navbar-brand col-8">${company.name}</a>
                    <div>
                        <p class="max-user">Max users: ${company.maxUser}</p>
                    </div>
                    <div>
                        <p class="current-user"></p>
                    </div>
                </nav>
                <table class="table" style="margin: 0">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">№</th>
                            <th scope="col">Name</th>
                            <th scope="col">LastName</th>
                            <th scope="col">Role</th>
                            <th scope="col">id</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="alert-wrap">
            </div>
            `
        )
    }

    static renderList(body) {
        body.innerHTML = '';
        this.userList.map(i => body.appendChild(i));
    }

    static getTBody() {
        let tBody = document.querySelector('tbody');
        return tBody;
    }

    static showCurrentUsers() {
        document
            .querySelector('.current-user')
            .textContent = `Current users: ${this.userList.length + 1}`
    }

    static createUserHandler(user) {
        let elemTr = document.createElement(`tr`);
        elemTr.classList.add(`${user.getUserId}`);
        elemTr.classList.add(`user-list`);

        elemTr.innerHTML = `
            <th class='numb' scope="row">${this.userList.length + 1}</th>
            <td class="name">${user.getName}</td>
            <td class="last-name">${user.getLastName}</td>
            <td class="role">${user.getRole}</td>
            <td class="id">${user.getUserId}</td>
            <td class="btn-wrap">
                <div type="button" class="btn btn-danger"></div>
            </td>
            
        `

        this.userList.push(elemTr);

        this.renderList(this.getTBody())
        this.showCurrentUsers();

        elemTr
            .querySelector('.btn-danger')
            .addEventListener('click', () => {
                admin.deleteUser(user.getUserId);
            });
    }

    static deleteUserHandler(id) {
        let findUser = this.userList.find(i => i.className === `${id} user-list`);
        console.log(findUser);
        this.userList = this.userList.filter(i => i !== findUser)
        this.renderList(this.getTBody())
        document
            .querySelectorAll('.numb')
            .forEach((i,index) => i.textContent = index + 1)
        this.showCurrentUsers()
    }

    static changeUserHandler(user) {
        this.userList.forEach(i => {
            if(i.className === `data-${user.getUserId}`) {
                i.innerHTML = ` 
                <th class='numb' scope="row">${this.userList.length + 1}</th>
                <td class="name">${user.getName}</td>
                <td class="last-name">${user.getLastName}</td>
                <td class="role">${user.getRole}</td>
                <td class="id">${user.getUserId}</td>
                <td class="btn-wrap">
                    <div type="button" class="btn btn-danger"></div>
                </td>
            `
                i.querySelector('.btn-danger').addEventListener('click', () => admin.deleteUser(user.getUserId));
            }
        });

        this.renderList(this.getTBody())
    }

    static noSpaceHandler(message) {
        let wrapDanger = document.querySelector('.alert-wrap')
        let alertDanger  = document.createElement('div')
        alertDanger.setAttribute('class', 'alert-danger')
        alertDanger.textContent = message
        wrapDanger.appendChild(alertDanger)

        setTimeout(() => alertDanger.remove(), 2000)
    }
}